package delete;

import java.sql.*;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		
		String url = "jdbc:mysql://localhost:3306/wed?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			//Initialisaton du driver de la SGBD
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url, user, password);

			PreparedStatement ps = connexion.prepareStatement("delete from user where id = ? ");
							  ps.setInt(1,1);
						  	  int rows = ps.executeUpdate();
			
			if (rows == 1) {
				System.out.println("the line was deleted successfully!");
			} else if (rows ==0)
				System.out.println("the line is already deleted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
