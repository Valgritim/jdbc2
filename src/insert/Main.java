package insert;

import java.sql.*;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		String url = "jdbc:mysql://localhost:3306/wed?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			
			//Initialisaton du driver de la SGBD
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url,user,password);
			
			String request = "INSERT INTO user(nom,prenom,sexe,rue,codePostal,ville) VALUES (?,?,?,?,?,?)";
			PreparedStatement ps = connexion.prepareStatement(request,PreparedStatement.RETURN_GENERATED_KEYS);
							  ps.setString(1, "Jedusor");
							  ps.setString(2, "Tom");
							  ps.setString(3, "M");
							  ps.setString(4, "tree street");
							  ps.setString(5, "0980BF");
							  ps.setString(6, "Godric's Holow");
							  ps.executeUpdate();
							  ResultSet resultat = ps.getGeneratedKeys();
					
			
			if(resultat.next())
							
				System.out.println("L'id g�n�r� pour cette personne est: " + resultat.getInt(1));
			
		} catch(SQLException e) {
				e.printStackTrace();
		}
	}
}