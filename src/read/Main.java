package read;

import java.sql.*;


public class Main {

	public static void main(String[] args) throws ClassNotFoundException {

	String url = "jdbc:mysql://localhost:3306/wed?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
	String user = "root";
	String password = "root";

	try
	{

		//Initialisaton du driver de la SGBD
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connexion = DriverManager.getConnection(url, user, password);
		Statement statement = connexion.createStatement();

		String request = "SELECT * FROM user";
		ResultSet result = statement.executeQuery(request);
		
		while(result.next()) {
			int idUser = result.getInt("id");
			String nomUser = result.getString("nom");
			String prenomUser = result.getString("prenom");
			String sexeUser = result.getString("sexe");
			String rueUser = result.getString("rue");
			String codePostalUser = result.getString("codePostal");
			String villeUser = result.getString("ville");
			System.out.println("[" + idUser + "]" + " " + nomUser + " " + prenomUser + " " + sexeUser + " " + rueUser + " " + codePostalUser + " " + villeUser);
		}
		
	}catch(SQLException e){
		e.printStackTrace();
	}
}
	}
