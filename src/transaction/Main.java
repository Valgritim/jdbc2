package transaction;

import java.sql.*;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException{
		String url = "jdbc:mysql://localhost:3306/wed?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, password);
			conn.setAutoCommit(false);
			String sql = "INSERT INTO user(nom,prenom,sexe,rue,codePostal,ville) VALUES (?,?,?,?,?,?)";
			PreparedStatement state = conn.prepareStatement(sql);
							state.setString(1, "Wisley");
							state.setString(2, "Ginny");
							state.setString(3, "F");
							state.setString(4, "tree street");
							state.setString(5, "0980BF");
							state.setString(6, "Godric's Holow");
			int rowsInserted = state.executeUpdate();
			
			if (rowsInserted > 0) {
				System.out.println("L'insertion est prise en compte");
			}
			
			ResultSet result = state.executeQuery("SELECT * FROM user");
			//result.first();
			
			while (result.next()) {
				System.out
						.println("Nom : " + result.getString("nom") + " - Prenom : " + result.getString("prenom"));
			}
			
			conn.commit();
			
			result.close();
			state.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}