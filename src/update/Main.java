package update;

import java.sql.*;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException {

		String url = "jdbc:mysql://localhost:3306/wed?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";

		try {
			//Initialisaton du driver de la SGBD
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url, user, password);

			String request = "UPDATE user SET nom=?,prenom=? WHERE id=?";
			PreparedStatement ps = connexion.prepareStatement(request, PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, "Wicko");
			ps.setString(2, "Johnny");
			ps.setInt(3, 1);
			ps.executeUpdate();
			System.out.println("User updat� ");			

			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
